import "./Branches.css"
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { LanguageContext } from "../contexts/language";

export default function Branches() {
  const {t} = useContext(LanguageContext)
  const [data, setData] = useState([]);
  useEffect(() => {
    axios
      .get("https://cc-cached.devsmith4289.workers.dev/api/v1/public/branches")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
  return (
      <table>
        <thead>
          <tr>
            <th>{t('Branch:id')}</th>
            <th>{t('Branch:name')}</th>
            <th>{t('Branch:Phonenumber')}</th>
            <th>{t('Branch:Photo')}</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => (
            <tr key={item.id}>
              <td>{item.id}</td>
              <td>{item.name}</td>
              <td>{item.phoneNumber}</td>
              <td>
                <img alt={item.branchImageUrl} src={item.branchImageUrl}></img>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
  );
}
