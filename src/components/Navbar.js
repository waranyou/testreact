import { Link } from "react-router-dom";
import "./Navbar.css";

import { LanguageContext } from "../contexts/language";
import { useContext } from "react";

export default function Navbar() {

  const {setLanguage, t } = useContext(LanguageContext);
  return (
    <nav>
      {/* <a href="/">หน้าเเรก</a>
            <a href="/about">เกี่ยวกับเรา</a> */}

      <Link to="/" className="logo">
        <h3>{t('Home:logo')}</h3>
      </Link>
      <button onClick={() => setLanguage('th')}>ไทย</button>
      /
      <button onClick={() => setLanguage('en')}>Eng</button>
      {/* <div  onClick={() => setLanguage('th')}>
        ไทย
      </div>
      /
      <div  onClick={() => setLanguage('en')}>
        Eng
      </div> */}
      <Link to="/">{t('Home:LinkFirst')}</Link>
      <Link to="/Branches">{t('Home:LinkSecond')}</Link>
    </nav>
  );
}
