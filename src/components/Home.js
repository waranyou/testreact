import { LanguageContext } from "../contexts/language";
import React, { useContext } from "react";
export default function Home(){
    const {t} = useContext(LanguageContext)
    return(
        <>
        <p>{t('Home:title')}</p>
        </>
    )
}